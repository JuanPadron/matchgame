﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MatchGame.Controls
{
    /// <summary>
    /// Interaction logic for Card.xaml
    /// </summary>
    public partial class Card : UserControl, INotifyPropertyChanged
    {
      
        public Card()
        {
            InitializeComponent();
            this.Loaded += Card_Loaded;
            DataContext = this;
        }
   
        public event PropertyChangedEventHandler PropertyChanged;

        public GameWindow Owner { get; set; }
    
        public enum eState 
        { 
            Inactive, Idle, Flipped, Matched
        }

        private eState state { get; set; } = eState.Inactive;

        public eState State {
            get { return state; }
            set
            {
                if (value != state)
                {
                    state = value;
                    Interactable = (state == eState.Idle);
                    Show = (state == eState.Flipped || state == eState.Matched);
                    NotifyPropertyChanged("State");
                }
            }
        }

        public bool Show 
        {
            set
            {
                if (value)
                {
                    lblSymbol.Visibility = Visibility.Visible;
                    imgCard.Visibility = Visibility.Hidden;
                }
                else
                {
                    lblSymbol.Visibility = Visibility.Hidden;
                    imgCard.Visibility = Visibility.Visible;
                }
            }
        }

        private bool interactable;

        public bool Interactable
        {

            get { return interactable; }
            set
            {
                if (value != interactable)
                {
                    interactable = value;
                    NotifyPropertyChanged("Interactable");
                }
            }

        }

        private string symbol;

        public string Symbol
        {
            get { return symbol; }
            set
            {
                if (value != symbol)
                {
                    symbol = value;
                    NotifyPropertyChanged("Symbol");
                }
            }
        }
      
        private void NotifyPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        

        private void Card_Loaded(object sender, RoutedEventArgs e)
        {
            Owner = (GameWindow)Window.GetWindow(this);
            Owner.RegisterCard(this);
        }

        private void btnCard_Click(object sender, RoutedEventArgs e) 
        {
            SoundPlayer playSound = new SoundPlayer(@"C:\Users\JPadron\source\repos\MatchGame\Resources\beep-01a.wav");
            playSound.Play();
            this.State = eState.Flipped;
            Owner.SelectCard(this);
        }


    }
}
