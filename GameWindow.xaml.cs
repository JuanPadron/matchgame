﻿using MatchGame.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace MatchGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {

        private Random random = new Random();
        private DispatcherTimer timer;


        public GameWindow()
        {
            InitializeComponent();
            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += TimerTick;
        }

        private Card card1 = null;
        private Card card2 = null;

        private List<string> symbols = new List<string>()
        {
            "!", "!", "N", "N", ",", ",",
            "b", "b", "v", "v", "w", "w",   };

        List<Card> cards = new List<Card>();

        public void RegisterCard(Card card)
        {
            card.State = Card.eState.Idle;
            int r = random.Next(symbols.Count);
            card.Symbol = symbols[r];
            symbols.RemoveAt(r);
            cards.Add(card);
        }

        private void TimerTick(object sender, EventArgs e)
        {
            timer.Stop();
            foreach (Card cardx in cards)
            {
                if (cardx.State != Card.eState.Matched)
                {
                    cardx.State = Card.eState.Idle;
                }
            }
            card1 = null;
            card2 = null;
        }

        public void SelectCard(Card card) 
        { 
            if (card1 == null)
            {
                card1 = card;
            }
            else
            {
                card2 = card;

	            if (card1.Symbol.Equals(card2.Symbol)) // check for matching cards (card1, card2) compare card symbols
	            {
                    // set card1 and card2 State to Matched
                    card1.State = Card.eState.Matched;
                    card2.State = Card.eState.Matched;
                    // set card1 and card2 to null
                    card1 = null;
                    card2 = null;
                }
	            else
	            {
		            // disable all cards, so they can’t be selected for a second
		            // this allows the player to see the flipped card for a moment
		            foreach (Card cardx in cards) // iterate through all cards
		            {
			            // if card State is Idle
                        if (cardx.State == Card.eState.Idle)
			            {
                            // set card State to inactive
                            cardx.State = Card.eState.Inactive;
                        }
		            }
                    // start timer, after a second the TimerTick is called and the cards reset
                    timer.Start();
	            }
            }
        }
    }
}
